#include "tetris.h"

int main(int argc, char const *argv[])
{
    init_tetris();

    start_tetris();

    stop_tetris();

    return 0;
}
